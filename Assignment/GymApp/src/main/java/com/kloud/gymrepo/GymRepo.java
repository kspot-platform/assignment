package com.kloud.gymrepo;

import java.util.List;

import com.kloud.exceptions.GymNotFoundException;
import com.kloud.model.Gym;

public interface GymRepo {
	void addMember(Gym Gym);
	void updateMember(int id,double weight);
	void deleteMember(int id);
	List<Gym>findAll();
	List<Gym>findByName(String name) throws GymNotFoundException;
	List<Gym>findByWeightbetween(double weight1,double weight2)throws GymNotFoundException;
}