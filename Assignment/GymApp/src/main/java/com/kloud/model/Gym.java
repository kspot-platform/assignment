package com.kloud.model;

public class Gym {
	
	private int id;
	private String name;
	private double fees;
	private double weight;
	private double height;

	public Gym() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Gym(int id, String name, double fees, double weight, double height) {
		super();
		this.id = id;
		this.name = name;
		this.fees = fees;
		this.weight = weight;
		this.height = height;
	}

	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getFees() {
		return fees;
	}

	public void setFees(double fees) {
		this.fees = fees;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Gym [id=" + id + ", name=" + name + ", Fees=" + fees + ", weight=" + weight + ", height=" + height
				+ "]";
	}

}
